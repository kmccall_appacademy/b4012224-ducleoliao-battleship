class HumanPlayer
  attr_reader :name

  def initialize(name)
    @name = name
  end

  def get_play
    print "Where do you want to place ship"
    user_input = gets.chomp
    user_input.split(",").map! { |str| str.to_i }
  end

end
