class Board
attr_accessor :grid

  def initialize(grid=Board.default_grid)
    @grid = grid
  end

  def self.default_grid
    @grid = Array.new(10) { Array.new(10) }
  end

  def [](pos)
    row, col = pos
    @grid[row][col]
  end

  def []=(pos, mark)
    row, col = pos
    @grid[row][col] = mark
  end

  def count
    ship_counts = 0
    @grid.flatten.each { |ship| ship_counts += 1 if ship == :s }
    ship_counts
  end

  def empty?(pos=nil)
    if pos
      self[pos].nil?
    else
      if self.count > 0
        return false
      end
      true
    end
  end

  def full?
    @grid.flatten.none? { |ship| ship.nil? }
  end

  def place_random_ship
    raise "The board is already full!" if self.full?
    row = rand(@grid.size)
    col = rand(@grid.size)
    @grid[row][col] = :s
  end

  def won?
    @grid.flatten.none? { |ship| ship == :s }
  end

end
